from datetime import datetime
from django.shortcuts import render
from cart.cart import Cart
from cart.forms import PlaceChooseForm
from .models import Order, OrderItem
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def order_create(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = PlaceChooseForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
    order = Order.objects.create(user = request.user,
                                created=datetime.now(),
                                status = 1,
                                placement = cd['placement'])
                                
    for item in cart:
        OrderItem.objects.create(order=order,
                                         drink=item['product'],
                                         cost=item['price'],
                                         quantity=item['quantity'])
    cart.clear()

    return render(request, 'orders/order/created.html',
                          {'order': order, 'title': "Order was created"})
