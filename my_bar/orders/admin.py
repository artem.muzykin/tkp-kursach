from django.contrib import admin

# Register your models here.
from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_fields = ['drink', 'order']



class OrderAdmin(admin.ModelAdmin):
    list_display = ['id',  'user',
                    'created', 'status']
    list_filter = ['user', 'created', 'status']
    inlines = [OrderItemInline]

admin.site.register(Order, OrderAdmin)
