from tkinter import CASCADE
from django import db
from django.db import models
from userMenu.models import Drink
from account.models import User

# Create your models here.

STATUS_CHOICES = (
    (1, 'Created'),
    (2, 'Accepted'),
    (3, 'Finished'),
    (4, 'Canceled'),
    (5, 'Closed')
)

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField()
    status = models.IntegerField(choices=STATUS_CHOICES)
    placement = models.IntegerField()
    total_cost = 0
    class Meta:
        db_table = 'Orders'

    def __str__(self):
        return 'Order {}'.format(self.id)




class OrderItem(models.Model):
    #order = models.ManyToManyField(Order)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    drink = models.ForeignKey(Drink, on_delete=models.CASCADE)
    cost = models.FloatField()
    quantity = models.IntegerField(default=1)

    class Meta:
        db_table = 'OrderItem'

    def __str__(self):
        return '{}'.format(self.drink)

    def get_cost(self):
        return self.cost * self.quantity