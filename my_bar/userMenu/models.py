from email.mime import image
from tokenize import ContStr
from turtle import title
from django.db import models

# Create your models here.


class MenuElement(models.Model):
    available=  models.BooleanField()
    cost=       models.IntegerField()
    title=      models.CharField(max_length=50)
    content=    models.CharField(max_length=255)
    image=      models.ImageField()

class Categories(models.Model):
    title=      models.CharField(max_length=255)
    availible=  models.BooleanField()

    def __str__(self):
        return self.title
    
    class Meta:
        db_table = "Categories"

class Drink(models.Model):
    category=   models.ForeignKey(Categories, on_delete=models.PROTECT)
    name=       models.CharField(max_length=255)
    content=    models.CharField(max_length=255)
    cost=       models.FloatField()
    volume=     models.IntegerField()
    image=      models.ImageField()
    availible=  models.BooleanField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = "Drink"

