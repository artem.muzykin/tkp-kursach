from gc import collect
from multiprocessing import context
from turtle import clear
from django.http import HttpResponse
from django.shortcuts import render
from cart.forms import CartAddProductForm
from userMenu.forms import *

from userMenu.models import Categories, Drink, MenuElement
# Create your views here.


def cat(request):
    categories = Categories.objects.raw('SELECT * FROM Categories')

    context = {
        'cat' : categories,
        'title' : "Soc Bar",
    }
    return render(request,'userMenu/categories.html', context=context)

def show_category(request, category):
    model = Drink.objects.raw('SELECT * FROM Drink')
    categories = Categories.objects.raw('SELECT * FROM Categories')
    form = CartAddProductForm()
    title = "undefine"
    for x in categories:
        if(x.pk == category):
            title = x.title
            break

    array = []
    for x in model:
        if(x.category.pk == category):
            array.append(x)

    context = {
        'title': title,
        'menu':  array,
        'cart_product_form': form,
    }
    return render(request, 'userMenu/menu.html', context=context)

def index(request): #Http request
    menu = MenuElement.objects.raw('SELECT * FROM Menu')
    context = {
        'menu' : menu,
        'title' : "Soc Bar",
    }
    return render(request,'userMenu/index.html', context=context)

def login(request):
    return render(request, 'userMenu/login.html', { 'title' : 'Login'})


#404 Handler
def pageNotFound(request, exception):
    return HttpResponse("404 page not found")