from django import template
from userMenu.models import *

register = template.Library()

@register.simple_tag
def get_categories():
    return  Categories.objects.raw('SELECT * FROM Categories')

@register.simple_tag
def add(value, adder):
    return value + adder
