from unicodedata import category
from django.contrib import admin

# Register your models here.
from account.models import Profile
from .models import *

class DrinkAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'category', 'cost', 'volume', 'availible')
    list_display_links = ('id', 'name')
    search_fields = ('name', 'category')
    list_editable = ('availible', 'cost')
    list_filter = ('availible', 'cost', 'category')

class CategotyAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'availible')

class ProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'name',  'age', 'sex', 'email', 'phone']

admin.site.register(Categories, CategotyAdmin)
admin.site.register(Drink, DrinkAdmin)



admin.site.register(Profile, ProfileAdmin)