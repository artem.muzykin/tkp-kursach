from django.urls import path 

from .views import *

urlpatterns = [
    path('', cat, name = 'home'),
    path('login/', login, name = 'login'),
    path('cat/', cat, name = 'categoriew'),
    path('<int:category>', show_category, name = 'menu'),
]