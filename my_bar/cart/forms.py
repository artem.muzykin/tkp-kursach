from django import forms

PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 21)]
PLACE_CHOICES = [(i, str(i)) for i in range(1,12)]

class CartAddProductForm(forms.Form):
    quantity = forms.TypedChoiceField(choices=PRODUCT_QUANTITY_CHOICES, coerce=int)
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)

class PlaceChooseForm(forms.Form):
    placement = forms.TypedChoiceField(choices=PLACE_CHOICES, coerce=int)