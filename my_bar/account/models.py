from django.db import models
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from .managers import UserManager
# Create your models here.



class User(AbstractBaseUser, PermissionsMixin):
    is_active = models.BooleanField(('active'), default=True)
    login = models.CharField(max_length=30, unique=True)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    is_staff = models.BooleanField(default=False)
        

    objects = UserManager()

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = []

    class Meta:
        db_table = "Users"

class Profile(models.Model):
    name =  models.CharField(max_length=50)
    age =   models.DateField()
    sex =   models.CharField(max_length=10, blank=True)
    email = models.TextField(blank=True)
    phone = models.CharField(max_length=15, blank=True)

    user =  models.OneToOneField(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)

    def __str__(self):
        return 'Profile for user {}'.format(self.user.login)

    class Meta:
        db_table = "Profiles"