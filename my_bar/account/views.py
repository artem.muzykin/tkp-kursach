from multiprocessing import context
import profile
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, UserRegistrationForm, ProfileRegistrationForm
from .models import Profile
from orders.models import Order, OrderItem

from .forms import LoginForm, UserRegistrationForm, ProfileEditForm
# Create your views here.
def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    
    context = {
        'form' : form,
        'title': "User login"
    }
    return render(request, 'account/login.html',context=context)

@login_required
def dashboard(request):
    orders = Order.objects.filter(user = request.user.pk)
    ids    = orders.values_list('pk', flat=True)
    items = OrderItem.objects.filter(order__in = ids)
    profiles = Profile.objects.filter(user = request.user)
    profile = profiles[0]
    for order in orders:
        for item in items:
            if item.order == order:
                order.total_cost += item.get_cost()
    context = {
        'section': 'dashboard',
        'orders': orders,
        'items': items,
        'title': "Dashboard",
        'profile': profile
    }
    return render(request, 'account/dashboard.html', context=context)

@login_required
def accept_decline(request, id, ans):
    if request.method == 'POST':
        orders = Order.objects.filter(id = id)
        order = orders[0]
        if ans:
            order.status = 5
            order.save()
        else:
            order.status = 4
            order.save()
    
    return redirect('dashboard')

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        profile_form = ProfileRegistrationForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            
            new_profile = profile_form.save(commit=False)
            new_profile.user_id = new_user.pk
            new_profile.save()
            return render(request, 'account/register_done.html', {'new_user': new_user, 'profile_form': profile_form, 'title' : "Registration done"})
    else:
        user_form = UserRegistrationForm()
        profile_form = ProfileRegistrationForm()
    return render(request, 'account/register.html', {'user_form': user_form, 'profile_form': profile_form, 'title' : "Registration"})


@login_required
def edit(request):
    profiles = Profile.objects.raw('SELECT * FROM Profiles')
    prof = None
    for p in profiles:
        if(p.user_id == request.user.pk):
            prof = p
    if request.method == 'POST':
        
        profile_form = ProfileEditForm(instance=prof, data=request.POST, files=request.FILES)
        if   profile_form.is_valid():
            profile_form.save()
            return redirect('dashboard')
    else:
        profile_form = ProfileEditForm(instance=prof)
        return render(request,
                      'account/edit.html',
                      {'profile_form': profile_form, 'title': "Edit profile"})

@login_required
def history(request):
    orders = Order.objects.filter(user=request.user)
    ids    = orders.values_list('pk', flat=True)
    items = OrderItem.objects.filter(order__in = ids)
    for order in orders:
        for item in items:
            if item.order == order:
                order.total_cost += item.get_cost()
    return render(request, 'account/history.html', {'title': "Orders history", 'orders':orders, 'items': items})