from django.urls import re_path, path
from . import views
from django.contrib.auth import views as auth_views
urlpatterns = [
    # previous login view
    # url(r'^login/$', views.user_login, name='login'),

    # login / logout urls
    re_path(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    re_path(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    re_path(r'^logout-then-login/$', auth_views.logout_then_login, name='logout_then_login'),

    re_path(r'^password-change/$', auth_views.PasswordChangeView.as_view(), name='password_change'),
    re_path(r'^password-change/done/$', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    re_path(r'^password-reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    re_path(r'^password-reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    re_path(r'^password-reset/confirm/(?P<uidb64>[-\w]+)/(?P<token>[-\w]+)/$', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    re_path(r'^password-reset/complete/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    re_path(r'^$', views.dashboard, name='dashboard'),
    re_path(r'^history/$', views.history, name='history'),
    path(r'^$<int:id>/<int:ans>', views.accept_decline, name='acpdec'),
    re_path(r'^register/$', views.register, name='register'),
    re_path(r'^edit/$', views.edit, name='edit'),
]