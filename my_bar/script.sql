CREATE TABLE Menu (
	id INT NOT NULL,
	available BOOLEAN, 
	cost INT NOT NULL,
	title VARCHAR(50),
	content VARCHAR(255),
	image_link VARCHAR(255),
	
	PRIMARY KEY(id)	
);